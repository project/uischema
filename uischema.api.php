<?php

/**
 * @file
 * Hooks specific to the uischema.org module.
 */

/**
 * @addtogroup uischema
 */

/**
 * Respond to node being parsed into valid schema.org data.
 *
 * This hooks allows modules to alter the valid JSON a node is being parsed into.
 *
 * @param array $fields
 *   The fields being used in the JSON data
 * @param \Drupal\Core\Entity\ContentEntityBase $entity
 *   The entity being parsed.
 * @param bool $skip_list_fields
 *   Whether list fields should be ignored in the serialisation.
 */
function hook_uischema_entity_to_json_alter(array &$fields, \Drupal\Core\Entity\ContentEntityBase $entity, bool $skip_list_fields) {}

/**
 * @} End of "addtogroup uischema".
 */
