<?php

namespace Drupal\uischema\Form;

use Drupal\Core\Url;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\RemoveCommand;
use Drupal\Core\Ajax\PrependCommand;
use Drupal\Core\Ajax\ChangedCommand;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Ajax\CloseModalDialogCommand;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBuilderInterface;

use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Drupal\media\Entity\Media;

use Drupal\uischema\Service\SchemaService;
use Drupal\uischema\Service\EntityService;
use Drupal\uischema\Service\FormatService;
use Drupal\uischema\Plugin\Field\FieldWidget\WebPageElementWidget;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * WebPageElementForm class.
 */
class WebPageElementForm extends FormBase {
    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container) {
        $form = new static();
        $form->setRequestStack($container->get('request_stack'));
        $form->setStringTranslation($container->get('string_translation'));
        $form->setMessenger($container->get('messenger'));
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'web_page_element_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, $options = []) {
        $build_info = $form_state->getBuildInfo(); 

        $field_name = $build_info['field_name'];
        $field_delta = $build_info['field_delta'];
        $field_type = $build_info['field_type'];
        $field_data = $build_info['field_data'];

        $post_url = Url::fromRoute('uischema.form.webpageelement.submit');

        $form['#action'] = $post_url->toString();
        $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
        $form['#attached']['library'][] = 'uischema/webpageelement';

        $form['status_messages'] = [
            '#type' => 'status_messages',
        ];

        if(!$field_type) { 
            drupal_set_message(t('No type selected'), 'warning');
            return $form;
        }

        // Save some form state info in hidden fields in the form
        // NOTE: This is a temporary workaround, hopefully 
        $hidden_inputs = [
            'field_type' => $field_type,
            'field_name' => $field_name,
            'field_delta' => $field_delta,
        ];

        foreach($hidden_inputs as $key => $value) {
            $form[$key] = [ '#type' => 'hidden', '#name' => $key, '#default_value' => $value, ];
        }

        // Populate data fields using the schema        
        $schema = SchemaService::getSchema($field_type, $field_data);
        
        if(!$schema) {
            $form['warning']['#title'] = t('Schema not found') . ': ' . $field_type;
            return;
        }

        $form['field_data'] = [
            '#type' => 'container',
        ];

        foreach($schema as $key => $definition) {
            $field = $this->getDataField(
                isset($schema['@i18n']['en'][$key]) ? $schema['@i18n']['en'][$key] : null,
                $definition,
                isset($field_data[$key]) ? $field_data[$key] : null,
                $key,
                'field_data[' . $key . ']'
            );

            if(!$field) { continue; }

            $form['field_data'][$key] = $field;
        }

        // Submit button
        // NOTE: This cannot be inside an "actions" container because of #tabledrag
        $form['submit'] = [
            '#type' => 'submit',
            '#default_value' => t('Apply'),
            '#attributes' => [
                'class' => [ 'use-ajax', 'ui-dialog--web-page-element-f__apply' ],
            ],
            '#ajax' => [
                'event' => 'click',
                'url' => $post_url,
            ],
        ];


        return $form;
    }

    /**
     * Converts an input name to an id string
     *
     * @return string
     */
    private function inputNameToId(string $input_name) {
        $input_id = str_replace('_', '-', $input_name);
        $input_id = str_replace('[', '-', $input_id);
        $input_id = str_replace(']', '', $input_id);

        return 'web-page-element-f--' . $input_id;
    }

    /**
     * Converts an input name to an array of keys
     *
     * @return array
     */
    private function inputNameToArray(string $input_name) {
        return preg_split('/[\[\]]+/', str_replace('field_data[', '', $input_name), -1, PREG_SPLIT_NO_EMPTY);
    }

    /**
     * Validate data
     */
    private function validateData(array $field_data, string $field_type): array {
        $schema = SchemaService::getSchema($field_type, $field_data);
        unset($schema['@i18n']);

        foreach($schema as $key => $definition) {
            if($key[0] === '@') { continue; }

            $field_data[$key] = EntityService::getDataValue(
                isset($field_data[$key]) ? $field_data[$key] : null,
                $definition,
                true
            );
        }

        return $field_data;
    }

    /**
     * Apply correct sort order to fields according to weights
     */
    private function sortData(array $field_data, array $field_weights): array {
        $is_numeric_array = true;

        foreach($field_data as $key => $value) {
            if(!is_numeric($key)) {
                $is_numeric_array = false;
            }

            if(
                !isset($field_data[$key]) || !is_array($field_data[$key]) ||
                !isset($field_weights[$key]) || !is_array($field_weights[$key])
            ) { continue; }

            $field_data[$key] = $this->sortData($field_data[$key], $field_weights[$key]);
        }

        if($is_numeric_array) {
            uksort($field_data, function($a, $b) use ($field_weights) {
                if(
                    !isset($field_weights[$a]['weight']) ||
                    !is_numeric($field_weights[$a]['weight']) ||
                    !isset($field_weights[$b]['weight']) ||
                    !is_numeric($field_weights[$b]['weight'])
                ) {
                    return 0;
                }
                
                $a_weight = (int) $field_weights[$a]['weight'];
                $b_weight = (int) $field_weights[$b]['weight'];

                $sort = 0;
                
                if($a_weight < $b_weight) {
                    $sort = -1;
                } else if($a_weight > $b_weight) {
                    $sort = 1;
                }

                return $sort;
            });

            $field_data = array_values($field_data);
        }

        return $field_data;
    }

    /**
     * AJAX callback handler for the "add item" button
     */
    public function addArrayItemAjax() {
        $field_type = $_POST['field_type']; 
        $field_data = $this->validateData($_POST['field_data'], $field_type);

        // Locate the array in question and append an empty item to it
        $values = &$field_data;
        
        $array_field_name = $this->inputNameToArray($_GET['array_field_name']);

        foreach($array_field_name as $i => $n) {
            if(!isset($values[$n])) {
                $values[$n] = [];
            }

            $values = &$values[$n];
        }

        $values[] = null;

        // Recreate the modal in a new response
        $field_title = EntityService::getFieldItemTitle($field_data, $field_type);
        $field_name = $_POST['field_name'];
        $field_delta = $_POST['field_delta']; 
        
        $response = new AjaxResponse();

        $modal_form_state = new FormState();
        $modal_form_state->disableCache();
        $modal_form_state->addBuildInfo('field_name', $field_name);
        $modal_form_state->addBuildInfo('field_delta', $field_delta);
        $modal_form_state->addBuildInfo('field_type', $field_type);
        $modal_form_state->addBuildInfo('field_data', $field_data);

        $modal_form = \Drupal::formBuilder()->buildForm('Drupal\uischema\Form\WebPageElementForm', $modal_form_state);

        $modal_command = new OpenModalDialogCommand($field_title, $modal_form, [
            'dialogClass' => 'ui-dialog--web-page-element-f',
        ]);

        $response->addCommand($modal_command);
        
        return $response;
    }

    /**
     * AJAX callback handler for the "remove item" button
     */
    public function removeArrayItemAjax() {
        $field_type = $_POST['field_type']; 
        $field_data = $this->validateData($_POST['field_data'], $field_type);

        // Locate the array in question and remove the indicated item
        $values = &$field_data;
        
        $array_field_name = $this->inputNameToArray($_GET['array_field_name']);
        $array_item_index = $_GET['array_item_index'];
        
        foreach($array_field_name as $i => $n) {
            if(!isset($values[$n])) {
                $values[$n] = [];
            }

            $values = &$values[$n];
        }

        array_splice($values, $array_item_index, 1);
        
        // Recreate the modal in a new response
        $field_title = EntityService::getFieldItemTitle($field_data, $field_type);
        $field_name = $_POST['field_name'];
        $field_delta = $_POST['field_delta']; 
        
        $response = new AjaxResponse();

        $modal_form_state = new FormState();
        $modal_form_state->disableCache();
        $modal_form_state->addBuildInfo('field_name', $field_name);
        $modal_form_state->addBuildInfo('field_delta', $field_delta);
        $modal_form_state->addBuildInfo('field_type', $field_type);
        $modal_form_state->addBuildInfo('field_data', $field_data);
        $modal_form_state->addBuildInfo('field_data', $field_data);

        $modal_form = \Drupal::formBuilder()->buildForm('Drupal\uischema\Form\WebPageElementForm', $modal_form_state);

        $modal_command = new OpenModalDialogCommand($field_title, $modal_form, [
            'dialogClass' => 'ui-dialog--web-page-element-f',
        ]);

        $response->addCommand($modal_command);
        
        return $response;
    }
    
    /**
     * AJAX callback handler for the "apply" button
     */
    public function submitFormAjax() {
        $field_type = $_POST['field_type'];
        $field_name = $_POST['field_name'];
        $field_delta = $_POST['field_delta'];
        $field_data_selector = 'textarea[name="' . $field_name . '[' .  $field_delta . '][field_data]"]';
        $field_title_selector = 'label[data-drupal-selector="' . str_replace('_', '-', $field_name) . '-' . $field_delta . '-title"]';
        $field_data = isset($_POST['field_data']) ? $_POST['field_data'] : [];
        $field_weights = isset($_POST['field_weights']) ? $_POST['field_weights'] : [];

        $field_data_sorted = $this->sortData($field_data, $field_weights);
        $field_data_validated = $this->validateData($field_data_sorted, $field_type);
        $field_data_json = json_encode($field_data_validated, JSON_FORCE_OBJECT + JSON_UNESCAPED_SLASHES);
        $field_data_base64 = base64_encode($field_data_json);
        $field_title = EntityService::getFieldItemTitle($field_data_validated, $field_type);
        
        $response = new AjaxResponse();
        $response->addCommand(new HtmlCommand($field_title_selector, $field_title));
        $response->addCommand(new HtmlCommand($field_data_selector, $field_data_base64));
        $response->addCommand(new ChangedCommand($field_data_selector));
        
        $response->addCommand(new CloseModalDialogCommand());

        return $response;
    }

    /**
     * {@inheritdoc}.
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {}

    /**
     * Gets a data field
     *
     * @return array
     */
    private function getDataField($i18n, $definition, $value, $title, $input_name) {
        if($title[0] === '@') { return null; }

        $field = [];

        $definition = SchemaService::fieldDefinitionSanityCheck($definition);

        if(!$definition || !isset($definition['@type'])) { return null; }

        if(isset($definition['@min'])) { $field['#attributes']['min'] = $definition['@min']; }
        if(isset($definition['@max'])) { $field['#attributes']['max'] = $definition['@max']; }

        $field['#title'] = isset($i18n['@name']) ? $i18n['@name'] : $title;
        $field['#description'] = isset($i18n['@description']) ? $i18n['@description'] : '';
        
        $field['#name'] = $input_name; 
        $field['#attributes']['name'] = $input_name; 

        // NOTE: We can't use custom ids because of CKEditor
        //$field['#attributes']['id'] = $this->inputNameToId($input_name);
        
        // Translate field types to inputs
        switch($definition['@type']) {
            // Schema
            default:
                $schema = SchemaService::getSchema($definition['@type'], $value);

                if($schema) {
                    $field['#type'] = 'fieldgroup';

                    unset($field['#name']);
                    unset($field['#attributes']['name']);

                    // Get field title
                    if(
                        isset($schema['@label']) &&
                        isset($value[$schema['@label']]) &&
                        is_string($value[$schema['@label']])
                    ) {
                        $field['#title'] = $value[$schema['@label']];
                    }

                    foreach($schema as $k => $d) {
                        if($k[0] === '@') { continue; }

                        $array_item_field = $this->getDataField(
                            isset($schema['@i18n']['en'][$k]) ? $schema['@i18n']['en'][$k] : null,
                            $d,
                            isset($value[$k]) ? $value[$k] : null,
                            $k,
                            $input_name . '[' . $k . ']'
                        );

                        if(!$array_item_field) { continue; }

                        $field[$k] = $array_item_field;
                    }

                } else {
                    $field['#type'] = 'label';
                    $field['#title'] = 'Unknown data type "' . $definition['@type'] . '"';

                }
                break;

            // Array
            case 'ItemList':
                // Establish number of array items
                $array_item_count = 0;
                
                if(isset($value['numberOfItems'])) {
                    $array_item_count = (int) $value['numberOfItems'];
                } else if(is_array($value)) {
                    $array_item_count = sizeof($value);
                }

                if(isset($definition['@min']) && $array_item_count < $definition['@min']) {
                    $array_item_count = $definition['@min'];
                }
                
                if(isset($definition['@max']) && $array_item_count > $definition['@max']) {
                    $array_item_count = $definition['@max'];
                }

                // Establish item type
                // TODO: This way, we're only supporting 1 type of array item. This should be fixed
                $array_item_type = isset($definition['@options'][0]) ? $definition['@options'][0] : null;

                // Build table
                $field['#type'] = 'table';
                $field['#tree'] = true;
                $field['#attributes'] = [
                    'class' => [ 'field-multiple-table', 'responsive-enabled', 'web-page-element-f__array' ],
                ];
                $field['#header'] = [
                    'title' => [
                        'colspan' => 10,
                        'data' => $field['#title'],
                    ],
                ];
                $field['#tabledrag'] = [
                    [
                        'action' => 'order',
                        'relationship' => 'sibling',
                        'group' => 'web-page-element-f__array__item__weight',
                    ],
                ];

                for($i = 0; $i < $array_item_count; $i++) {
                    $array_item_value = $value[$i];
                    $weight_input_name = str_replace('field_data', 'field_weights', $input_name);

                    $field[$i] = [
                        '#attributes' => [
                            'class' => [ 'draggable', 'web-page-element-f__array__item' ],
                        ],
                        '#weight' => $i,
                        'tabledrag-handle' => [
                            '#wrapper_attributes' => [
                                'class' => [ 'field-multiple-drag', 'web-page-element-f__array__item__drag' ],
                            ],
                        ],
                        'content' => [
                            '#wrapper_attributes' => [
                                'class' => [ 'web-page-element-f__array__item__content' ],
                            ],
                            'data' => $this->getDataField(
                                isset($i18n['@options'][0]) ? $i18n['@options'][0] : null,
                                $array_item_type,
                                $array_item_value,
                                null,
                                $input_name . '[' . $i . ']'
                            )
                        ],
                        'remove_item' => [
                            '#wrapper_attributes' => [
                                'class' => [ 'web-page-element-f__array__item__remove' ],
                            ],
                            'data' => [
                                '#type' => 'button',
                                '#default_value' => t('Remove item'),
                                '#attributes' => [
                                    'class' => ['use-ajax'],
                                ],
                                '#name' => $input_name . '[' . $i . '][remove_item]',
                                '#ajax' => [
                                    'url' => Url::fromRoute('uischema.form.webpageelement.removearrayitem', [
                                        'array_field_name' => $input_name,
                                        'array_item_index' => $i,
                                    ]),
                                ],
                            ],
                        ],
                        'weight' => [
                            '#type' => 'weight',
                            '#title' => t('Weight'),
                            '#title_display' => 'invisible',
                            '#default_value' => $i,
                            '#name' => $weight_input_name . '[' . $i . '][weight]',
                            '#attributes' => [
                                'class' => [ 'web-page-element-f__array__item__weight' ],
                            ],
                        ],
                    ];
                }
                
                $field['tools'] = [
                    '#attributes' => [
                        'class' => [ 'web-page-element-f__array__tools' ],
                    ],
                    'add_item' => [
                        '#wrapper_attributes' => [
                            'colspan' => 10,
                        ],
                        'data' => [
                            '#type' => 'button',
                            '#default_value' => t('Add another item'),
                            '#attributes' => [
                                'class' => ['use-ajax'],
                            ],
                            '#name' => $input_name . '[add_item]',
                            '#ajax' => [
                                'url' => Url::fromRoute('uischema.form.webpageelement.addarrayitem', [
                                    'array_field_name' => $input_name,
                                ]),
                            ],
                        ],
                    ],
                ];
                break;

            // Nested structure
            case 'StructuredValue':
                $field['#type'] = 'fieldgroup';

                foreach($definition as $k => $d) {
                    $dict_field = $this->getDataField(
                        isset($i18n[$k]) ? $i18n[$k] : null,
                        $d,
                        isset($value[$k]) ? $value[$k] : null,
                        $k,
                        $input_name . '[' . $k . ']'
                    );

                    if(!$dict_field) { continue; }
                    
                    $field[$k] = $dict_field;
                }
                break;

            // Time            
            case 'Date':
                $field['#type'] = 'date';
                $field['#default_value'] = $value;
                $field['#date_date_format'] = 'Y-m-d';
                $field['#attributes']['data-drupal-date-format'] = ['Y-m-d'];
                break;
            
            case 'DateTime':
                $field['#type'] = 'datetime';
                $field['#default_value'] = $value;
                $field['#date_time_format'] = 'H:i';
                $field['#date_date_format'] = 'Y-m-d';
                $field['#attributes']['data-drupal-time-format'] = ['H:m'];
                $field['#attributes']['data-drupal-date-format'] = ['Y-m-d'];
                break;

            // Boolean
            case 'Boolean':
                if(is_numeric($value)) {
                    $value = $value === 1 || $value === '1';
                }

                $field['#type'] = 'checkbox';
                $field['#default_value'] = $value;
                break;

            // Media reference
            case 'AudioObject':
            case 'DataDownload':
            case 'ImageObject':
            case 'MediaObject':
            case 'VideoObject':
                $field['#type'] = 'entity_autocomplete';
                $field['#target_type'] = 'media';

                // Determine which media type to use based on the bundle label
                $media_bundles = \Drupal::entityManager()->getBundleInfo('media');
                $target_bundles = [];

                if($definition['@type'] === 'MediaObject') {
                    $target_bundles = array_keys($media_bundles);
                } else {
                    foreach($media_bundles as $bundle_id => $bundle_info) {
                        if($definition['@type'] !== $bundle_info['label']) { continue; }

                        $target_bundles[] = $bundle_id;
                    }
                }

                $field['#selection_settings'] = [
                    'target_bundles' => $target_bundles,
                    'match_limit' => 1000,
                ];

                if(isset($value['identifier'])) {
                    $field['#default_value'] = Media::load($value['identifier']);
                
                } else if(is_numeric($value)) {
                    $field['#default_value'] = Media::load($value);
                
                } else {
                    $field['#default_value'] = null;
                
                }

                break;

            // Node reference
            case 'CreativeWork':
                $field['#type'] = 'entity_autocomplete';
                $field['#default_value'] = Node::load($value);
                $field['#target_type'] = 'node';

                $node_target_bundles = null;

                if(isset($definition['@options']) && is_array($definition['@options'])) {
                    $node_target_bundles = [];

                    foreach($definition['@options'] as $option) {
                        if(!is_string($option)) { continue; }

                        $node_target_bundles[] = FormatService::toSnakeCase($option);
                    }
                }

                $field['#selection_settings'] = [
                    'match_limit' => 1000,
                    'target_bundles' => $node_target_bundles
                ];
                break;

            // Picker
            case 'Enumeration':
                $field['#type'] = 'select';
                $field['#multiple'] = !isset($definition['@max']) || $definition['@max'] !== 1;

                $options = [];

                if(isset($definition['@options'])) {
                    $definition_options = array_values($definition['@options']);
                    $i18n_options = array_values($i18n['@options']);

                    foreach($definition_options as $i => $k) {
                        if(isset($i18n_options[$i])) {
                            $options[$k] = $i18n_options[$i];
                        } else {
                            $options[$k] = $k;
                        }
                    }
                }

                $field['#options'] = $options;
                $field['#default_value'] = $value;
                break;

            // Text
            case 'Text':
            case 'URL':
                $field['#type'] = 'textfield';
                $field['#default_value'] = $value;
                break;
                
            case 'MultiLineText':
                $field['#type'] = 'textarea';
                $field['#default_value'] = $value;
                break;

            case 'RichText':
                $field['#type'] = 'text_format';
                $field['#format'] = 'full_html';
                $field['#allowed_formats'] = [ 'full_html' ];
                $field['#default_value'] = $value;
                break;
            
            // Number
            case 'Number':
                if(is_bool($value)) {
                    $value = $value ? 1 : 0;
                }

                $field['#type'] = 'number';
                $field['#default_value'] = $value;
                break;
            
        }

        return $field;
    }
}
