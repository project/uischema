<?php 

namespace Drupal\uischema\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Drupal\Core\Controller\ControllerBase;

use Drupal\node\Entity\Node;

use Drupal\uischema\Service\EntityService;
use Drupal\uischema\Service\MenuService;

class NodeController extends ControllerBase {
    /**
     * Returns a node by URL or id
     *
     * @return JsonResponse
     */
    public function node(Request $request) {
        $id = $request->query->get('id');
        $url = $request->query->get('url');
        $type = $request->query->get('type');

        try {
            $node = null;

            if($id) {
                $node = Node::load($id);
            } else if($url) {
                $node = EntityService::getNodeByUrl($url);
            } else {
                throw new \Exception('Either parameter "id" or "url" is required', 400);
            }
            
            $node_type = $node ? $node->get('type')->get(0)->get('target_id')->getString() : '';

            if(!$node || ($type && $type !== $node_type)) {
                throw new \Exception('Page not found', 404);
            }

            $json = EntityService::entityToJson($node);

            if(!$json) {
                throw new \Exception('Could not parse content', 500);
            }

            $json['@context'] = 'http://schema.org';

            $json['breadcrumb'] = MenuService::getNodeBreadcrumbAsJson($node);

            return new JsonResponse($json);

        } catch(\Exception $e) {
            $code = $e->getCode();

            if($code < 400) { $code = 502; }

            return new JsonResponse(['error' => $e->getMessage(), 'code' => $code], $code);

        }
    }
    
    /**
     * Returns a menu structure
     *
     * @return JsonResponse
     */
    public function menu(Request $request) {
        try {
            $menu_name = $request->query->get('name');
            $menu_root_item_name = $request->query->get('root');
            
            if(empty($menu_name)) {
                throw new \Exception('Parameter "menu" is required', 400);
            }
            
            $menu = MenuService::getMenuItemsAsJson($menu_name, $menu_root_item_name);

            if(!$menu) {
                if($menu_root_item_name) {
                    throw new \Exception('No root item by name "' . $menu_root_item_name . '" in menu structure "' . $menu_name . '" was found', 404);
                }
                
                throw new \Exception('Menu structure by name "' . $menu_name . '" could not be found', 404);
            }

            return new JsonResponse($menu);
        
        } catch(\Exception $e) {
            $code = $e->getCode();

            if($code < 400) { $code = 502; }

            return new JsonResponse(['error' => $e->getMessage()], $code);

        }
    }
}
