<?php 

namespace Drupal\uischema\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Drupal\Core\Controller\ControllerBase;

use Drupal\uischema\Service\SchemaService;

class SchemaController extends ControllerBase {
    /**
     * Shows all schemas
     */
    public function all(Request $request) {
        return new JsonResponse(SchemaService::getAllSchemas());
    }
}
