<?php 

namespace Drupal\uischema\Service;

use Drupal\Core\Menu\MenuLinkTreeElement;
use Drupal\Core\Menu\MenuLinkInterface;
use Drupal\Core\Menu\MenuTreeParameters;

use Drupal\node\Entity\Node;

use Drupal\menu_link_content\Entity\MenuLinkContent;

/**
 * Services related to the menu structure
 */
class MenuService {
    /**
     * Gets a menu link by the node it references
     *
     * @return MenuLinkContent
     */
    public static function getMenuItemContentByNode(?Node $node) {
        if(!$node) { return null; }

        $items = \Drupal::entityTypeManager()
            ->getStorage('menu_link_content')
            ->loadByProperties([
                'link.uri' => 'entity:node/' . $node->id(),            
            ]);
        
        if(!$items) { return null; }

        foreach($items as $item) {
            if($item->getMenuName() !== 'main') { continue; }

            return $item;
        }

        return null;
    }

    /**
     * Gets the parent of a menu link
     *
     * @return MenuLinkContent
     */
    public static function getMenuItemContentParent(?MenuLinkContent $menu_item_content) {
        if(!$menu_item_content) { return null; }
        
        $parent_uuid = str_replace('menu_link_content:', '', $menu_item_content->getParentId());

        $parents = \Drupal::entityTypeManager()
            ->getStorage('menu_link_content')
            ->loadByProperties([
                'uuid' => $parent_uuid,
            ]);

        if(!$parents || sizeof($parents) < 1) { return null; }

        return reset($parents);
    }

    /**
     * Generates a node URL based on menu structure
     *
     * @return string
     */
    public static function getNodeUrlByMenuStructure(?Node $node) {
        if(!$node) { return null; }
        
        $menu_link = self::getMenuItemContentByNode($node);

        if(!$menu_link) { return null; }

        $url_names = [];

        // Ascend up the menu tree
        while($menu_link) {
            // Prepend the URL name to the list
            $url_name = FormatService::toUrlString($menu_link->getTitle());
            
            array_unshift($url_names, $url_name);
            
            // Get next menu item
            $menu_link = self::getMenuItemContentParent($menu_link);
        }

        return '/' . implode('/', array_filter($url_names));
    }

    /**
     * Gets a node breadcrumb
     *
     * @return array ItemList
     */
    public static function getNodeBreadcrumbAsJson(?Node $node, bool $include_self = false) {
        $menu_link = self::getMenuItemContentByNode($node);

        if(!$menu_link) { return null; }

        $items = [];

        while($menu_link) {
            // Get node reference
            $route_parameters = $menu_link->getUrlObject()->getRouteParameters();
            $menu_link_node_id = isset($route_parameters['node']) ? $route_parameters['node'] : null;

            // Prepend the item to the list
            if($include_self || $menu_link_node_id != $node->id()) {
                $menu_link_node = Node::load($menu_link_node_id);

                $item = [
                    'identifier' => $menu_link->uuid(),
                    'name' => $menu_link->getTitle(),
                    'url' => EntityService::getEntityUrl($menu_link_node),
                ];

                array_unshift($items, $item);
            }
            
            // Get next menu item
            $menu_link = self::getMenuItemContentParent($menu_link);
        }

        if(sizeof($items) < 1) { return null; }

        $item_list = FormatService::toItemList($items);

        $item_list['@type'] = 'BreadcrumbList';

        return $item_list;
    }

    /**
     * Parses a menu item into JSON
     *
     * @return array
     */
    public static function menuItemToJson(?MenuLinkTreeElement $menu_item) {
        if(!$menu_item || !$menu_item->link) { return null; }
        
        $menu_item_content_id = $menu_item->link->getPluginDefinition()['metadata']['entity_id'];
        $menu_item_content = \Drupal::entityTypeManager()->getStorage('menu_link_content')->load($menu_item_content_id);

        if(!$menu_item_content || !$menu_item_content->isEnabled()) { return null; }

        $node_id = $menu_item_content->getUrlObject()->getRouteParameters()['node'];
        $node = Node::load($node_id);

        return [
            '@type' => 'StructuredValue',
            'identifier' => $menu_item_content->uuid(),
            'name' => $menu_item_content->getTitle(),
            'weight' => $menu_item_content->getWeight(),
            'url' => $node ? EntityService::getEntityUrl($node) : null,
            'children' => self::menuItemsToJson($menu_item->subtree)
        ];
    }

    /**
     * Parses menu items into JSON
     *
     * @return array
     */
    public static function menuItemsToJson(array $menu_items) {
        foreach($menu_items as $i => $menu_item) {
            $menu_items[$i] = self::menuItemToJson($menu_item);
        }
        
        $menu_items = array_filter($menu_items);

        usort($menu_items, function($a, $b) {
            $a = $a['weight'];
            $b = $b['weight'];

            return $a < $b ? -1 : 1;
        });

        if(sizeof($menu_items) < 1) { return null; }

        return FormatService::toItemList($menu_items);
    }

    /**
     * Gets all links in a menu
     *
     * @return ItemList
     */
    public static function getMenuItemsAsJson(string $menu_name, ?string $root_menu_item_name = '') {
        $menu_parameters = new MenuTreeParameters();
        $menu_parameters->setMaxDepth(100);
        
        $menu_items = \Drupal::menuTree()->load($menu_name, $menu_parameters);

        // If root menu item name is specified, only list this one item's children
        if($root_menu_item_name) {
            foreach($menu_items as $menu_item) {
                $menu_item = self::menuItemToJson($menu_item);

                if(!$menu_item || $menu_item['name'] !== $root_menu_item_name) { continue; }
                
                return FormatService::toItemList($menu_item['children']);
            }

            return null;
        }

        // If not specified, just list all menu items
        return self::menuItemsToJson($menu_items);
    }
}
