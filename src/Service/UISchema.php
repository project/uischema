<?php 

namespace Drupal\uischema\Service;

/**
 * A helper class for providing convenient shortcuts
 */
class UISchema {
    public static function toItemList($array) {
        return FormatService::toItemList($array);
    }
    
    public static function entityToJson($entity, $skip_list_fields = false) {
        return EntityService::entityToJson($entity, $skip_list_fields, true);
    }
    
    public static function getEntityUrl($entity) {
        return EntityService::getEntityUrl($entity);
    }
    
    public static function getFieldItemValue($item) {
        return EntityService::getFieldItemValue($item);
    }
    
    public static function getNodeByUrl($url) {
        return EntityService::getNodeByUrl($url);
    }

    public static function getMenuItemsAsJson($menu_name, $menu_root_item_name = '') {
        return MenuService::getMenuItemsAsJson($menu_name, $menu_root_item_name);
    }

    public static function getNodeBreadcrumbAsJson($node) {
        return MenuService::getNodeBreadcrumbAsJson($node);
    }
    
    public static function getAllSchemas($include_abstract) {
        return SchemaService::getAllSchemas($include_abstract);
    }
    
    public static function getSchema($type, $data) {
        return SchemaService::getSchema($type, $data);
    }
}
