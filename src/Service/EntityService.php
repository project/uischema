<?php

namespace Drupal\uischema\Service;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Field\Plugin\Field\FieldType\BooleanItem;
use Drupal\Core\Language\LanguageInterface;

use Drupal\node\Entity\Node;
use Drupal\media\Entity\Media;
use Drupal\file\Entity\File;
use Drupal\user\Entity\User;
use Drupal\image\Entity\ImageStyle;

use Drupal\file\Plugin\Field\FieldType\FileItem;
use Drupal\link\Plugin\Field\FieldType\LinkItem;

use Drupal\uischema\Plugin\Field\FieldType\WebPageElement;

/**
 * A service class for handling entities
 */
class EntityService {
    /**
     * Gets a uischema.org version of a field item
     *
     * @return array
     */
    public static function getFieldItemValue(?FieldItemBase $item) {
        if(!$item) { return null; }

        if($item instanceof WebPageElement) {
            return self::getDataValue($item->getData(), $item->getType(), false);
        }

        if($item instanceof FileItem) {
            if(isset($item->getValue()['target_id'])) {
                $file = File::load($item->getValue()['target_id']);

                if(!$file) { return null; }

                return $file->url();
            }

            return null;
        }

        if($item instanceof EntityReferenceItem) {
            $entity_reference = $item->getValue();

            if(!$entity_reference || !isset($entity_reference['target_id'])) { return ''; }

            $entity_id = $entity_reference['target_id'];    
            $entity_type = $item->getFieldDefinition()->getSettings()['target_type'];

            if(!$entity_type) { return ''; }

            // Special cases
            switch($entity_type) {
                case 'media':
                    $entity_type = 'MediaObject';
                    break;

                default:
                    $entity_type = 'CreativeWork';
                    break;
            }

            return self::getDataValue($entity_id, $entity_type, false);
        }

        if($item instanceof LinkItem) {
            return [
                '@type' => 'StructuredValue',
                'text' => $item->get('title')->getString(),
                'href' => $item->getUrl()->toString(),
            ];
        }

        if($item instanceof BooleanItem) {
            return $item->getString() === '1';
        }

        // Field type not recognised, get the value as text
        $value = $item;
        
        if(isset($item->getValue()['value'])) {
            $value = $item->getValue()['value'];
        
        } else {
            $value = $item->getString();
        
        }

        // If the value contains HTML, process it like rich text
        if(strpos($value, '</') !== false) {
            return self::getDataValue($value, 'RichText');
        }

        return $value;
    }

    /**
     * Adopts values from a field into the structure of a field definition
     *
     * @param {mixed} value
     * @param {mixed} definition
     * @param {bool} is_internal
     *
     * @return mixed
     */
    public static function getDataValue($value, $definition, bool $is_internal = false) {
        if(!$definition) { return null; }
        
        $definition = SchemaService::fieldDefinitionSanityCheck($definition);

        switch($definition['@type']) {
            // Schema
            default:
                $schema = SchemaService::getSchema($definition['@type'], $value);

                if(!$schema) { return $value; }
                unset($schema['@i18n']);

                if(!is_array($value)) { $value = []; }

                $untyped_value = $value;
                $value = [];

                foreach($schema as $k => $d) {
                    if($k[0] === '@') {
                        $value[$k] = $d;
                    
                    } else { 
                        $value[$k] = self::getDataValue(
                            isset($untyped_value[$k]) ? $untyped_value[$k] : null,
                            $d,
                            $is_internal
                        );
                    
                    }
                }

                if(isset($value['@process']) && is_callable($value['@process'])) {
                    try {
                        $value = call_user_func($value['@process'], $value);
                    } catch(\Exception $e) {
                        $value['@error'] = [
                            'type' => get_class($e),
                            'message' => $e->getMessage(),
                        ];
                    }
                }

                if(isset($value['@init'])) {
                    unset($value['@init']);
                }

                if(isset($value['@process'])) {
                    unset($value['@process']);
                }

                return $value;

            // Option picker
            case 'Enumeration':
                // Single value
                if(isset($definition['@max']) && $definition['@max'] === 1) {
                    if(is_array($value)) {
                        return reset($value);
                    }

                    return $value;
                
                }

                // Multiple values
                if(is_string($value)) {
                    $value = explode(',', $value);
                
                } else if(!is_array($value)) {
                    $value = [ $value ];

                }

                return $value;

            // Text
            case 'Text':
                return $value;
            
            case 'MultiLineText':
                return $is_internal ? $value : nl2br($value);
           
            case 'RichText':
                $html = str_replace(["\n", "\r"], '', isset($value['value']) ? $value['value'] : $value);

                $html = preg_replace_callback('/<drupal-media.*data-entity-uuid="([^"]+)"><\/drupal-media>/', function(array $matches) {
                    $uuid = end($matches);

                    $entity = \Drupal::service('entity.repository')->loadEntityByUuid('media', $uuid);

                    if(!$entity) { return ''; }

                    $json = self::entityToJson($entity);

                    switch($json['@type']) {
                        case 'VideoObject':
                            return '<video title="' . $json['name'] . '" controls src="' . $json['contentUrl'] . '" poster="' . $json['thumbnailUrl'] . '"></video>';
                        
                        case 'AudioObject':
                            return '<audio title="' . $json['name'] . '" src="' . $json['contentUrl'] . '"></audio>';

                        case 'ImageObject':
                            return '<img alt="' . $json['name'] . '" src="' . $json['thumbnailUrl'] . '">';
                    }
                    
                    return '';
                     
                }, $html);

                return $html;

            // Media reference
            case 'AudioObject':
            case 'DataDownload':
            case 'ImageObject':
            case 'MediaObject':
            case 'VideoObject':
                $media_id = FormatService::stringToId($value);

                if($is_internal) {
                    if(empty($media_id)) { return null; }

                    return (int) $media_id;
                }

                $media = Media::load($media_id);

                if(!$media) { return null; }

                return self::entityToJson($media);

            // Node reference
            case 'CreativeWork':
                $content_id = FormatService::stringToId($value);
                
                if($is_internal) {
                    return $content_id;
                }

                $content = Node::load($content_id);

                if(!$content) { return null; }

                return self::entityToJson($content, true, true);

            // Numbers
            case 'Number':
                return (float) $value;

            // Boolean
            case 'Boolean':
                return $value === true || $value === "true" || $value === 1 || $value === '1';

            // Array 
            case 'ItemList':
                $items = is_array($value) ? $value : [];

                foreach($items as $i => $v) {
                    $items[$i] = self::getDataValue(
                        $v,
                        isset($definition['@options'][0]) ? $definition['@options'][0] : null,
                        $is_internal
                    );
                }
                
                return $is_internal ? $items : FormatService::toItemList($items);

            // Nested structure
            case 'StructuredValue':
                if(!is_array($value)) { $value = []; }

                foreach($definition as $k => $d) {
                    if($k[0] === '@') { continue; }

                    $value[$k] = self::getDataValue(
                        isset($value[$k]) ? $value[$k] : null,
                        $d,
                        $is_internal
                    );
                }

                return $value;
        }
    }
    
    /**
     * Parses an entity
     *
     * @return array JSON
     */
    public static function entityToJson(?ContentEntityBase $entity, bool $skip_list_fields = false, bool $skip_hook = false) {
        if(!$entity) { return null; }

        $fields = [];
        $fields['identifier'] = (int) $entity->id();
        $fields['dateCreated'] = date('Y-m-d H:i:s', $entity->getCreatedTime());
        $fields['dateChanged'] = date('Y-m-d H:i:s', $entity->getChangedTime());

        $language = $entity->language();

        if(
            $language &&
            $language->getId() !== LanguageInterface::LANGCODE_NOT_APPLICABLE &&
            $language->getId() !== LanguageInterface::LANGCODE_NOT_SPECIFIED
        ) {
            $fields['inLanguage'] = $language->getId();
        }

        foreach($entity->getIterator() as $field) {
            $field_key = $field->getName();

            if(strpos($field_key, 'field_') !== 0) { continue; }

            if($entity instanceof Media) {
                $field_key = str_replace('field_media_', '', $field_key);
                $field_key = str_replace('video_file', 'video', $field_key);
                $field_key = str_replace('audio_file', 'audio', $field_key);
                $field_key = str_replace('image_file', 'image', $field_key);
            }
            
            $field_key = str_replace('field_', '', $field_key);

            $field_key = FormatService::toCamelCase($field_key);
            $field_cardinality = $field->getFieldDefinition()->getFieldStorageDefinition()->getCardinality();
            $field_value = [];

            if($skip_list_fields && $field_cardinality !== 1) { continue; }

            foreach($field->getIterator() as $item) {
                $field_value[] = self::getFieldItemValue($item);
            }
            
            if($field_cardinality === 1) {
                $field_value = reset($field_value);
            } else {
                $field_value = FormatService::toItemList($field_value);
            }

            $fields[$field_key] = $field_value;
        }
        
        // Ensure fields for nodes
        if($entity instanceof Node) {
            $fields['@type'] = FormatService::toPascalCase($entity->get('type')->get(0)->get('target_id')->getString());
            $fields['name'] = $entity->getTitle();
            $fields['url'] = self::getEntityUrl($entity);

        // Ensure fields for media
        } else if($entity instanceof Media) {
            $fields['name'] = $entity->getName();

            // Image
            if(strpos(strtolower($entity->bundle()), 'image') !== false) {
                $fields['@type'] = 'ImageObject';
                $fields['image'] = $fields['contentUrl'];

            // Video
            } else if(strpos(strtolower($entity->bundle()), 'video') !== false) {
                $fields['@type'] = 'VideoObject';
                $fields['video'] = $fields['contentUrl'];

            // Audio
            } else if(strpos(strtolower($entity->bundle()), 'audio') !== false) {
                $fields['@type'] = 'AudioObject';
                $fields['audio'] = $fields['contentUrl'];
                
            // Download
            } else if(strpos(strtolower($entity->bundle()), 'download') !== false) {
                $fields['@type'] = 'DataDownload';
                
            // Generic media object
            } else {
                $fields['@type'] = 'MediaObject';
            
            }

            // Main file
            $file_source = $entity->getSource();
            $source_field = $file_source->getConfiguration()['source_field'];
            
            $field_item = $entity->get($source_field)->first();

            if($field_item) {
                $file_id = $field_item->{$field_item->mainPropertyName()};
                $file = File::load($file_id);

                if($file) {    
                    // Main URL
                    $fields['contentUrl'] = $file->url();
                    
                    // Style URLs
                    foreach(ImageStyle::loadMultiple() as $style) {
                        $fields[$style->id() . 'Url'] = $style->buildUrl($file->getFileUri());
                    }
                }
            }

            // Thumbnail URL
            if(isset($fields['thumbnail']) && !empty($fields['thumbnail'])) {
                $fields['thumbnailUrl'] = $fields['thumbnail'];
            } else if(!isset($fields['thumbnailUrl']) || empty($fields['thumbnailUrl'])) {
                $fields['thumbnailUrl'] = $entity->get('thumbnail')->entity->url();
            }
            
        }

        // Invoke hook
        if(!$skip_hook) {
            try {
                \Drupal::moduleHandler()->alter('uischema_entity_to_json', $fields, $entity, $skip_list_fields);
            } catch(\Exception $e) {
                $fields['@error'] = $e->getMessage();
            }
        }

        return $fields;
    }

    /**
     * Gets an entity URL
     *
     * @return string
     */
    public static function getEntityUrl(?ContentEntityBase $entity) {
        // Get the specified or generated URL for a node entity
        if($entity instanceof Node) {
            // If a node is promoted to front page, its URL will always be "/"
            if($entity->isPromoted()) {
                return '/';
            }

            // Get the URL from cache
            $cid = 'uischema.node_urls.' . $entity->id();
            $cache = \Drupal::cache()->get($cid);

            if(isset($cache->data) && !empty($cache->data)) { return $cache->data; }
            
            // Get the URL from the path alias
            $url = \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $entity->id());

            // Get the URL from the menu structure 
            if(!$url || $url === '/node/' . $entity->id()) {
                $url = MenuService::getNodeUrlByMenuStructure($entity);
            }

            // Set the URL as /node_type/id
            if(!$url) { 
                $url = '/' . $entity->get('type')->get(0)->get('target_id')->getString() . '/' . $entity->id();
            }

            \Drupal::cache()->set($cid, $url, -1, [ 'node:' . $entity->id() ]);

            return $url;

        // Get the source file URL of a media entity
        } else if($entity instanceof Media) {
            try {
                $file_source = $entity->getSource();
                $source_field = $file_source->getConfiguration()['source_field'];
                
                $field_item = $entity->get($source_field)->first();

                if(!$field_item) { return null; }

                $file_id = $field_item->{$field_item->mainPropertyName()};
                $file = File::load($file_id);
                
                if(!$file) { return null; }

                return $file->url();
            
            } catch(\Exception $e) {
                return null;

            }

        }

        return null;
    }

    /**
     * Gets a node by URL
     * TODO: Include support for the "path" module, and only use these methods as fallbacks
     *
     * @return Node
     */
    public static function getNodeByUrl(?string $url) {
        if(!$url) { return null; }

        $url = FormatService::formatEntityUrl($url);

        // First check if we're being asked for the front page
        if($url === '/') {
            $promoted_node_ids = \Drupal::entityQuery('node')
                ->condition('promote', 1)
                ->condition('status', 1)
                ->execute();

            // NOTE: We could throw an exception here, if more than one node was found,
            // but it would be too disruptive for the end user, so we'll just use the first result
            $node_id = reset($promoted_node_ids);

            return Node::load($node_id);
        }

        // If not the front page, look through all other nodes
        $node_ids = \Drupal::entityQuery('node')
            ->condition('status', 1)
            ->execute();

        // Look up node URL in cache
        foreach($node_ids as $i => $node_id) {
            $cid = 'uischema.node_urls.' . $node_id;
            $cache = \Drupal::cache()->get($cid);

            if(!isset($cache->data) || FormatService::formatEntityUrl($cache->data) !== $url) { continue; }

            $node = Node::load($node_id);

            if(!$node) { continue; }

            return $node;
        }

        // Find URL by looking through all nodes
        foreach($node_ids as $i => $node_id) {
            $node = Node::load($node_id);

            if(!$node) { continue; }

            $node_url = self::getEntityUrl($node);

            if($node_url !== $url) { continue; }

            return $node;
        }

        return null;
    }

    /**
     * Gets the title of a field item
     *
     * @return string
     */
    public static function getFieldItemTitle(?array $field_data, ?string $field_type) {
        if(empty($field_type)) { return $field_type; }
        if(empty($field_data)) { return $field_type; }

        $schema = SchemaService::getSchema($field_type, $field_data);

        if(isset($field_data[$schema['@label']]) && !empty($field_data[$schema['@label']])) {
            return $field_data[$schema['@label']];
        }
        
        if(isset($schema['@i18n']['en']['@name']) && !empty($schema['@i18n']['en']['@name'])) {
            return $schema['@i18n']['en']['@name'];
        }

        return $field_type;
    }
}
