<?php

namespace Drupal\uischema\Service;

/**
 * A service for exposing schemas
 */
class SchemaService {
    /**
     * Performs a sanity check for a schema
     *
     * @return mixed
     */
    public static function fieldDefinitionSanityCheck($definition) {
        if(!$definition) { return null; }
        
        // Make sure that definition is an associative array with a type value
        if(is_string($definition)) {
            $definition = [
                '@type' => $definition,
            ];
        }

        // Check array type
        $is_associative_array = false;
        $is_indexed_array = false;

        if(is_array($definition)) {
            $is_associative_array = false;
            $is_indexed_array = true;
            
            foreach($definition as $k => $v) {
                if(!is_numeric($k)) {
                    $is_associative_array = true;
                    $is_indexed_array = false;
                    break;
                }
            }
        }

        if($is_associative_array && !isset($definition['@type'])) {
            $definition['@type'] = 'StructuredValue';
        }

        if($is_indexed_array) {
            $definition = [
                '@type' => 'ItemList',
                '@options' => $definition,
            ];
        }

        // If the @type is not a string, throw an error
        if(!is_string($definition['@type'])) {
            throw new \Exception('The @type field value must be a string, value was: ' . serialize($definition['@type']));
        }

        return $definition;
    }
    
    /**
     * Gets all schema types
     *
     * @return array
     */
    public static function getAllSchemaTypes() {
        $cache = \Drupal::cache()->get('uischema.schema_types');

        if($cache && isset($cache->data) && !empty($cache->data)) {
            return $cache->data;
        }
        
        $all_sites_path = DRUPAL_ROOT . '/sites/all/uischema/*.json';
        $current_site_path = DRUPAL_ROOT . '/' . \Drupal::service('site.path') . '/uischema/*.json';

        $filenames = [];

        $all_sites_filenames = @glob($all_sites_path);
        $current_site_filenames = @glob($current_site_path);

        if(is_array($all_sites_filenames)) {
            $filenames += $all_sites_filenames;
        }
        
        if(is_array($current_sites_filenames)) {
            $filenames += $current_sites_filenames;
        }

        $schema_types = [];

        foreach($filenames as $filename) {
            if(pathinfo($filename, PATHINFO_EXTENSION) !== 'json') { continue; }

            $schema_type = basename($filename, '.json');

            $schema_types[] = $schema_type;
        }

        $schema_types = array_unique($schema_types);

        \Drupal::cache()->set('uischema.schema_types', $schema_types);

        return $schema_types;
    }

    /**
     * Gets all schemas
     *
     * @return array
     */
    public static function getAllSchemas(bool $include_abstract = false) {
        $schemas = [];

        foreach(self::getAllSchemaTypes() as $type) {
            $schema = self::getSchema($type);

            if(!$schema) { continue; }
            if(!$include_abstract && !isset($schema['@parent']) || $schema['@parent'] === 'Thing') { continue; }

            $schemas[] = $schema;
        }

        return $schemas;
    }

    /**
     * Gets a single schema
     */
    public static function getSchema(string $type, $data = null): ?array {
        $cache = \Drupal::cache()->get('uischema.schemas.' . $type);

        $schema = null;
        
        $all_sites_path = DRUPAL_ROOT . '/sites/all/uischema/' . $type . '.json';
        $current_site_path = DRUPAL_ROOT . '/' . \Drupal::service('site.path') . '/uischema/' . $type . '.json';

        if($cache && isset($cache->data) && !empty($cache->data)) {
            $schema = $cache->data;
        
        } else {
            $schema = @file_get_contents($current_site_path);
            
            if(!$schema) {
                $schema = @file_get_contents($all_sites_path);
            }

            if(!$schema) { return null; }

            $schema = json_decode($schema, true);

            if(!$schema) {
                throw new \Exception('Bad JSON for schema "' . $type . '"', 502);
            }

            // Merge parent values
            $parent_type = isset($schema['@parent']) ? $schema['@parent'] : '';

            while($parent_type && $parent_type !== 'WebPageElement' && $parent_type !== 'Thing') {
                $parent_schema = self::getSchema($parent_type, $data);

                if(!$parent_schema) { break; }

                $schema = array_merge($parent_schema, $schema);

                $parent_type = isset($parent_schema['@parent']) ? $parent_schema['@parent'] : '';
            }
        
            \Drupal::cache()->set('uischema.schemas.' . $type, $schema);
        }

        // Run init method if available
        if(isset($schema['@init']) && is_callable($schema['@init'])) {
            try {
                $schema = call_user_func($schema['@init'], $schema, $data);
            } catch(\Exception $e) {
                $schema['@error'] = $e->getMessage();
            }
        }

        return $schema;
    }
}
