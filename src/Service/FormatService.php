<?php

namespace Drupal\uischema\Service;

/**
 * A service for turning some things into other things
 */
class FormatService {
    /**
     * Converts a regular array to an ItemList
     *
     * @param array items
     *
     * @return array
     */
    public static function toItemList(?array $items = []) {
        if(isset($items['itemListElement'])) { return $items; }
        
        if(!$items || !is_array($items)) { $items = []; }
        if(!self::isIndexedArray($items)) { $items = array_values($items); }

        foreach($items as $i => $item) {
            $items[$i] = [
                '@type' => 'ListItem',
                'position' => $i,
                'item' => $item,
            ];
        }

        return [
            '@type' => 'ItemList',
            'numberOfItems' => sizeof($items),
            'itemListElement' => $items,
        ];
    }
    
    /**
     * Converts a string to PascalCase
     */
    public static function toPascalCase(?string $string) {
        if(!$string) { return ''; }

        $string = str_replace([ '-', '_' ], ' ', $string);
        $string = ucwords($string);
        $string = str_replace(' ', '', $string);
    
        return $string;
    }
    
    /**
     * Converts a string to camelCase
     */
    public static function toCamelCase(?string $string) {
        if(!$string) { return ''; }

        $string = self::toPascalCase($string);
        $string = lcfirst($string);

        return $string;
    }

    /**
     * Converts a string to snake_case
     */ 
    public static function toSnakeCase(?string $string) {
        if(!$string) { return ''; }

        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $string, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        return implode('_', $ret);
    }
    
    /**
     * Converts a string to spinal-case
     */ 
    public static function toSpinalCase(?string $string) {
        if(!$string) { return ''; }

        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $string, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        return implode('-', $ret);
    }
    
    /**
     * Converts a string to a-url-string
     */ 
    public static function toUrlString(?string $string) {
        if(!$string) { return ''; }

        $matches = [];

        preg_match_all('!([A-Za-z0-9.]+)!', $string, $matches);
        
        $ret = $matches[0];

        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }

        return implode('-', $ret);
    }

    /**
     * Ensures the /url/format of a string
     *
     * @return string
     */
    public static function formatEntityUrl(?string $url) {
        if(!$url) { return null; }

        return strtolower('/' . implode('/', array_filter(explode('/', $url))));
    }

    /**
     * Checks if an array is indexed
     */
    public static function isIndexedArray($array) {
        return is_array($array) && array_values($array) === $array;
    }
    
    /**
     * Gets an entity id from a string
     *
     * @return int
     */
    public static function stringToId($value) {
        if(is_int($value)) {
            return $value;
        }
        
        if(is_numeric($value)) {
            return (int) $value;
        
        }
        
        if(is_string($value)) {
            preg_match('#\((.*?)\)#', $value, $match);

            if(isset($match[1]) && is_numeric($match[1])) {
                return (int) $match[1];
            }
        }

        return null;
    }

}
