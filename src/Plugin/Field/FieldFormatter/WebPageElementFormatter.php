<?php

namespace Drupal\uischema\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

use Drupal\uischema\Service\EntityService;

/**
 * Plugin implementation of the 'web_page_element_f' formatter.
 *
 * @FieldFormatter(
 *   id = "web_page_element_f",
 *   label = @Translation("Web page element"),
 *   field_types = {
 *     "web_page_element"
 *   }
 * )
 */
class WebPageElementFormatter extends FormatterBase {
    /**
     * {@inheritdoc}
     */
    public function settingsSummary() {
        $summary = [];
        $summary[] = $this->t('Displays a web page element');
        return $summary;
    }

    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode) {
        $element = [];

        foreach ($items as $delta => $item) {
            if($item->isEmpty()) { continue; }
            if(!$item->getType()) { continue; }
            
            $element[$delta]['#markup'] = '<pre>' . json_encode(EntityService::getFieldItemValue($item), JSON_PRETTY_PRINT + JSON_UNESCAPED_SLASHES + JSON_FORCE_OBJECT) . '</pre>';
        }

        return $element;
    }

}

