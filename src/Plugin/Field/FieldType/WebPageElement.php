<?php

namespace Drupal\uischema\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Form\FormStateInterface;

use Drupal\file\Entity\File;

use Drupal\uischema\Service\SchemaService;
use Drupal\uischema\Service\EntityService;

/**
 * Plugin implementation of the 'web_page_element' field type.
 *
 * @FieldType (
 *   id = "web_page_element",
 *   label = @Translation("Web page element"),
 *   description = @Translation("An element on a web page"),
 *   default_widget = "web_page_element_w",
 *   default_formatter = "web_page_element_f"
 * )
 */
class WebPageElement extends FieldItemBase {
    /**
     * Defines the database schema
     *
     * @return array
     */
    public static function schema(FieldStorageDefinitionInterface $field_definition) {
        return [
            'columns' => [
                'type' => [
                    'type' => 'varchar',
                    'length' => 255,
                ],
                'data' => [
                    'type' => 'text',
                ],
            ],
        ];
    }

    /**
     * Gets whether types can be selected
     *
     * @return bool
     */
    public function canPickType() {
        return empty($this->getSetting('type'));
    }

    /**
     * Gets the schema type of this module
     *
     * @return string
     */
    public function getType() {
        $type = $this->getSetting('type');

        if(!$type) {
            $type = $this->get('type')->getString();
        }

        return $type;
    }

    /**
     * Gets the schema fields
     *
     * @return array
     */
    public function getData() {
        $data = $this->get('data')->getValue();

        if(empty($data)) { return []; }

        $json = @json_decode($data, true);

        if($json && is_array($json)) { return $json; }

        return [];
    }

    /**
     * Checks if this field is empty
     *
     * @return bool
     */
    public function isEmpty() {
        if(empty($this->getType())) { return true; }

        return false;
    }

    /**
     * Defines the internal properties of this field
     *
     * @return array
     */
    public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
        $properties['type'] = DataDefinition::create('string')
            ->setLabel(t('Type'));
        
        $properties['data'] = DataDefinition::create('string')
            ->setLabel(t('Data'));

        return $properties;
    }
    
    /**
     * Overrides the field settings
     *
     * @return array
     */
    public static function defaultFieldSettings() {
        return [
            'type' => null,
        ] + parent::defaultFieldSettings();
    }

    /**
     * Overrides the field settings form
     *
     * @return array
     */
    public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
        $element = [];
        
        $schema_options = [
            null => $this->t('All types')
        ];

        foreach(SchemaService::getAllSchemas(true) as $schema) {
            $schema_options[$schema['@type']] = isset($schema['@i18n']['en']['@name']) ? $schema['@i18n']['en']['@name'] : $schema['@type'];
        }
        
        $element['type'] = [
            '#title' => $this->t('Type'),
            '#type' => 'select',
            '#options' => $schema_options,
            '#default_value' => $this->getSetting('type'),
        ];

        return $element;
    }
}

