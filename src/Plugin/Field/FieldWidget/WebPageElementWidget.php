<?php

namespace Drupal\uischema\Plugin\Field\FieldWidget;

use Drupal\Core\Url;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\RemoveCommand;
use Drupal\Core\Ajax\OpenModalDialogCommand;

use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Datetime\DrupalDateTime;

use Drupal\Component\Serialization\Json;

use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;

use Drupal\uischema\Service\SchemaService;
use Drupal\uischema\Service\EntityService;

/**
 * Plugin implementation of the 'web_page_element_w' widget.
 *
 * @FieldWidget (
 *   id = "web_page_element_w",
 *   label = @Translation("Web page element"),
 *   field_types = {
 *     "web_page_element"
 *   }
 * )
 */
class WebPageElementWidget extends WidgetBase {
    /**
     * Generates the form element.
     *
     * @return array
     */
    public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
        $field_name = $this->fieldDefinition->getName();
        $is_removed = $form_state->getUserInput()[$field_name][$delta]['is_removed'];
        
        $field_type = $form_state->getUserInput()[$field_name][$delta]['tools']['field_type'];

        // Field data fetched from backend
        if(!$field_type) {
            $field_type = $items[$delta]->getType(); 
        }

        $field_data_base64 = $form_state->getUserInput()[$field_name][$delta]['field_data'];
        
        // Field data fetched directly from input, relevant when adding/removing items
        if($field_data_base64) {
            $field_data_json = base64_decode($field_data_base64);
            $field_data = json_decode($field_data_json, true);

        // Field data fetched from backend
        } else {
            $field_data = $items[$delta]->getData();
            $field_data_json = json_encode($field_data, JSON_FORCE_OBJECT + JSON_UNESCAPED_SLASHES);
            $field_data_base64 = base64_encode($field_data_json);

        } 

        
        // Removed item tool elements
        $element['is_removed'] = [
            '#theme_wrappers' => [],
            '#type' => 'checkbox',
            '#value' => $is_removed,
            '#attributes' => [
                'id' => 'edit-' . str_replace('_', '-', $field_name) . '-' . $delta . '-tools-is-removed',
                'autocomplete' => 'off',
                'class' => ['field--widget-web-page-element-w__is-removed'],
            ],
        ];

        // Tools container
        $element['tools'] = [
            '#type' => 'container',
            '#attributes' => [
                'class' => ['field--widget-web-page-element-w__tools'],
            ],
        ];
        
        $element['tools']['title_removed'] = [
            '#type' => 'label',
            '#title' => '(' . t('Removed') . ')',
            '#attributes' => [
                'class' => ['field--widget-web-page-element-w__title-removed'],
            ],
        ];

        // Title
        $element['tools']['title'] = [
            '#type' => 'label',
            '#title' => $field_type ? $field_type : '(' . t('New') . ')',
            '#id' => str_replace('_', '-', $field_name) . '-' . $delta . '-title',
            '#attributes' => [
                'class' => ['field--widget-web-page-element-w__title'],
            ],
        ];

        // Get schema
        $schema = SchemaService::getSchema($field_type, $field_data);

        if($schema) {
            if(isset($schema['@i18n']['en']['@name'])) {
                $element['tools']['title']['#title'] = $schema['@i18n']['en']['@name'];
            }

            if(isset($field_data[$schema['@label']]) && !empty($field_data[$schema['@label']])) {
                $element['tools']['title']['#title'] = $field_data[$schema['@label']];
            }
        }

        // Item type
        if($items[$delta]->canPickType()) {
            $schema_options = [
                null => t('Select a type') . '...',
            ];

            foreach(SchemaService::getAllSchemas() as $schema) {
                $schema_options[$schema['@type']] = isset($schema['@i18n']['en']['@name']) ? $schema['@i18n']['en']['@name'] : $schema['@type'];
            }
            
            $element['tools']['field_type'] = [
                '#type' => 'select',
                '#options' => $schema_options,
                '#default_value' => $field_type,
                '#attributes' => [
                    'class' => ['field--widget-web-page-element-w__type'],
                ],
            ];

        } else {
            $element['tools']['field_type'] = [
                '#type' => 'hidden',
                '#default_value' => $field_type,
                '#attributes' => [
                    'class' => ['field--widget-web-page-element-w__type'],
                ],
            ];

        }

        // Edit item dialog
        $element['tools']['edit'] = [
            '#type' => 'button',
            '#value' => t('Edit'),
            '#name' => $field_name . '[' . $delta . '][edit]',
            '#ajax' => [
                'callback' => [$this, 'openModalAjax'],
                'event' => 'click',
            ],
            '#attributes' => [
                'class' => ['field--widget-web-page-element-w__edit'],
            ],
        ];
        
        // Remove item
        $element['tools']['remove'] = [
            '#type' => 'html_tag',
            '#tag' => 'label',
            '#value' => t('Remove'),
            '#attributes' => [
                'class' => ['button', 'field--widget-web-page-element-w__remove'],
                'for' => $element['is_removed']['#attributes']['id'],
            ],
        ];
        
        $element['tools']['restore'] = [
            '#type' => 'html_tag',
            '#tag' => 'label',
            '#value' => t('Restore'),
            '#attributes' => [
                'class' => ['button', 'field--widget-web-page-element-w__restore'],
                'for' => $element['is_removed']['#attributes']['id'],
            ],
        ];

        // Item data
        $element['field_data'] = [
            '#type' => 'textarea',
            '#value' => $field_data_base64,
            '#attributes' => [
                'autocomplete' => 'off',
                'class' => ['field--widget-web-page-element-w__data'],
            ],
        ];
        
        return $element;
    }

    /**
     * Ajax callback for the "edit" button
     */
    public function openModalAjax(array &$form, FormStateInterface $form_state) {
        $field_name = $this->fieldDefinition->getName();
        $button = $form_state->getTriggeringElement();
        $delta = $button['#array_parents'][2];
        $field_type = $form_state->getUserInput()[$field_name][$delta]['tools']['field_type'];
        $field_title = EntityService::getFieldItemTitle($field_data, $field_type);
        $field_data_base64 = $form_state->getUserInput()[$field_name][$delta]['field_data'];
        $field_data_json = base64_decode($field_data_base64);
        $field_data = json_decode($field_data_json, true);

        if(empty($title)) { $title = $field_type; }
        
        $modal_form_state = new FormState();
        $modal_form_state->addBuildInfo('field_name', $field_name);
        $modal_form_state->addBuildInfo('field_delta', $delta);
        $modal_form_state->addBuildInfo('field_type', $field_type);
        $modal_form_state->addBuildInfo('field_data', $field_data);
        $modal_form_state->setRebuild();

        $modal_form = \Drupal::formBuilder()->buildForm('Drupal\uischema\Form\WebPageElementForm', $modal_form_state);

        $modal_command = new OpenModalDialogCommand($field_title, $modal_form, [
            'dialogClass' => 'ui-dialog--web-page-element-f',
        ]);

        $response = new AjaxResponse();
        $response->addCommand($modal_command);

        return $response; 
    }

    /**
     * Performs a sanity check on the form values
     *
     * @return array
     */
    public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
        $massaged_values = [];
        $field_name = $this->fieldDefinition->getName();
        $user_inputs = $form_state->getUserInput()[$field_name];

        foreach($user_inputs as $delta => $value) {
            $is_removed = isset($value['is_removed']) && $value['is_removed'];
            $field_type = isset($value['tools']['field_type']) ? $value['tools']['field_type'] : null;

            if($is_removed || !$field_type) { continue; }
            
            $field_data_base64 = isset($value['field_data']) ? $value['field_data'] : [];
            $field_data_json = base64_decode($field_data_base64);

            $massaged_values[] = [
                'type' => $field_type,
                'data' => $field_data_json,
            ];
        }

        return $massaged_values;
    }
}
