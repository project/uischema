'use strict';

(function ($) {
    /**
     * NOTE: All this code is here because modals can't be nested.
     * Bug report here: https://www.drupal.org/project/drupal/issues/2741877
     */
	Drupal.behaviors.webpageElement = {
    	attach: function(context, settings) {
            // CKEditor fixes
            if(Drupal.ckeditor) {
                // When a modal is closed, reassign the background modal to the foreground, if available
                $(window).on('dialog:afterclose', function (e, dialog, $element) {
                    $('#drupal-modal-background').attr('id', 'drupal-modal');
                });

                // Override the open dialog method
                if(!Drupal.ckeditor.originalOpenDialog) {
                    Drupal.ckeditor.originalOpenDialog = Drupal.ckeditor.openDialog;
                }

                Drupal.ckeditor.openDialog = function openDialog(editor, url, existingValues, saveCallback, dialogSettings) {
                    // Assign the current modal to the background
                    $('#drupal-modal').attr('id', 'drupal-modal-background');
                  
                    // Call the original open dialog method (without the save callback)
                    Drupal.ckeditor.originalOpenDialog(editor, url, existingValues, null, dialogSettings);

                    // Override the save callback
                    Drupal.ckeditor.saveCallback = function(returnValues) {
                        saveCallback(returnValues);

                        // Look for nested link modal and close it
                        var $dialog = $('div[id^="drupal-dialog-editorlink-dialog"]').parent();
                    
                        if($dialog.hasClass('ui-dialog')) { $dialog.dialog().dialog('close'); }

                        // Look for nested media library modal and close it
                        $dialog = $('div[id^="drupal-dialog-media-libraryui"]').parent();
                        
                        if($dialog.hasClass('ui-dialog')) { $dialog.dialog().dialog('close') }
                        
                        // Reassign the background modal to the foreground
                        window.setTimeout(function() {
                            $('#drupal-modal-background').attr('id', 'drupal-modal');
                        }, 100);
                    };
                };
            }
     	}
	};
}(jQuery));
